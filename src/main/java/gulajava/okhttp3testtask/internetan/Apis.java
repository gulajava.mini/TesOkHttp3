package gulajava.okhttp3testtask.internetan;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class Apis {


    /**
     * API UNTUK DAFTAR PARTAI
     **/
    public static String getPartaiApis(String Apikey) {

        return "/partai/api/parties?apiKey=" + Apikey;
    }

    /**
     * API DAFTAR KANDIDAT
     **/
    public static String getKandiatApis(String apikey) {

        return "/berita-pilkada/api/candidates?apiKey=" + apikey;
    }

}
