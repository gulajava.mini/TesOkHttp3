package gulajava.okhttp3testtask.views;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import gulajava.okhttp3testtask.R;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class ViewHold {

    private View mView;

    private TextView mTextViewJudul = null;
    private TextView mTextViewKet = null;
    private ImageView mImageView = null;

    private TextView mTextViewId = null;
    private TextView mTextViewKandidat = null;


    public ViewHold(View view) {
        mView = view;
    }


    public TextView getTextViewJudul() {

        if (mTextViewJudul == null) {
            mTextViewJudul = (TextView) mView.findViewById(R.id.teks_namapartai);
        }

        return mTextViewJudul;
    }

    public TextView getTextViewKet() {

        if (mTextViewKet == null) {
            mTextViewKet = (TextView) mView.findViewById(R.id.teks_misipartai);
        }

        return mTextViewKet;
    }

    public ImageView getImageView() {

        if (mImageView == null) {
            mImageView = (ImageView) mView.findViewById(R.id.gambar);
        }

        return mImageView;
    }

    public TextView getTextViewId() {

        if (mTextViewId == null) {
            mTextViewId = (TextView) mView.findViewById(R.id.teks_id);
        }

        return mTextViewId;
    }

    public TextView getTextViewKandidat() {

        if (mTextViewKandidat == null) {
            mTextViewKandidat = (TextView) mView.findViewById(R.id.teks_kandidat);
        }

        return mTextViewKandidat;
    }
}
