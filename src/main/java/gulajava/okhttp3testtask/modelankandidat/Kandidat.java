package gulajava.okhttp3testtask.modelankandidat;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class Kandidat {

    private int id;
    private String name;

    public Kandidat() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
