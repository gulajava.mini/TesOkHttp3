package gulajava.okhttp3testtask.modelanpartai;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class Data {

    private ResultsPartai results;

    public Data() {
    }

    public ResultsPartai getResults() {
        return results;
    }

    public void setResults(ResultsPartai results) {
        this.results = results;
    }
}
