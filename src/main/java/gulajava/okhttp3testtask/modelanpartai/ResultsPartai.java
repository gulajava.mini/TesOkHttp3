package gulajava.okhttp3testtask.modelanpartai;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class ResultsPartai {


    private int count;
    private int total;
    private List<Partai> parties = new ArrayList<>();

    public ResultsPartai() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Partai> getParties() {
        return parties;
    }

    public void setParties(List<Partai> parties) {
        this.parties = parties;
    }
}


