package gulajava.okhttp3testtask.modelankandidat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class ResultsCandidat {

    private int count;
    private int total;
    private List<Kandidat> candidates = new ArrayList<>();

    public ResultsCandidat() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Kandidat> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Kandidat> candidates) {
        this.candidates = candidates;
    }
}
