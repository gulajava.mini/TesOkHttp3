package gulajava.okhttp3testtask.modelanpartai;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class JSONModelPartai {

    private Data data;

    public JSONModelPartai() {
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
